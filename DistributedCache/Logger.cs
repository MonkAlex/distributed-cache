﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace DistributedCache
{
  static class Logger
  {
    static ILogger LoggerImpl;

    public static void Debug(string message)
    {
      WriteExceptionToEventSource(() => LoggerImpl.Debug(message));
    }

    public static void Error(string message)
    {
      WriteExceptionToEventSource(() => LoggerImpl.Error(message));
    }

    public static void Error(Exception exception)
    {
      WriteExceptionToEventSource(() => LoggerImpl.Error(exception));
    }

    private static void WriteExceptionToEventSource(Action action)
    {
      try
      {
        action();
      }
      catch (System.Exception e)
      {
        EventLog.WriteEntry(nameof(DistributedCacheService), e.ToString(), EventLogEntryType.Error);
      }
    }

    static Logger()
    {
      var config = new LoggingConfiguration();
      var layout = @"${longdate} ${processid} ${threadid} ${assembly-version} ${level} ${message}${onexception:${newline}${exception:format=tostring}}";

      var fileTarget = new FileTarget();
      config.AddTarget("file", fileTarget);
      fileTarget.FileName = @"${basedir}/logs/${processname}.log";
      fileTarget.ArchiveFileName = "${basedir}/logs/${processname}.{#}.log";
      fileTarget.ArchiveNumbering = ArchiveNumberingMode.Date;
      fileTarget.ArchiveEvery = FileArchivePeriod.Day;
      fileTarget.ArchiveOldFileOnStartup = true;
      fileTarget.CreateDirs = true;
      fileTarget.Layout = layout;
      fileTarget.Encoding = Encoding.UTF8;
      var rule = new LoggingRule("*", LogLevel.Debug, fileTarget);
      config.LoggingRules.Add(rule);

      var consoleTarget = new ColoredConsoleTarget();
      config.AddTarget("console", consoleTarget);
      consoleTarget.Layout = layout;
      var rule2 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
      config.LoggingRules.Add(rule2);

      LogManager.Configuration = config;
      LogManager.ThrowExceptions = true;
      LoggerImpl = LogManager.GetLogger("default");
      Debug("===== START =====");
    }
  }
}
