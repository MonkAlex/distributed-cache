﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace DistributedCache.Models
{
  public class RemoteCache : ICache
  {
    protected string address;

    public Stream Get(string key)
    {
      using (var client = new HttpClient())
      {
        return client.GetAsync(address + key).Result.Content.ReadAsStreamAsync().Result;
      }
    }

    public void Set(string key, Stream value)
    {
      throw new NotImplementedException();
    }

    public void Remove(string key)
    {
      throw new NotImplementedException();
    }

    public bool Contains(string key)
    {
      try
      {
        using (var client = new HttpClient(){Timeout = TimeSpan.FromSeconds(2)})
        {
          return client.GetAsync(address + "contains=" + key).Result.Content.ReadAsStringAsync().Result.Contains("true");
        }
      }
      catch (Exception e)
      {
        Logger.Error(e);
        return false;
      }
    }

    public RemoteCache(string address)
    {
      if (!address.EndsWith("/"))
        address += "/";
      this.address = address;
    }
  }
}
