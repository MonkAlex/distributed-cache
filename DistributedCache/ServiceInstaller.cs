﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceProcess;

namespace DistributedCache
{
  [RunInstaller(true)]
  public partial class ServiceInstaller : System.Configuration.Install.Installer
  {
    public ServiceInstaller()
    {
      InitializeComponent();
      var installer = new System.ServiceProcess.ServiceInstaller();
      var processInstaller = new ServiceProcessInstaller();

      processInstaller.Account = ServiceAccount.LocalSystem;
      installer.StartType = ServiceStartMode.Automatic;
      installer.ServiceName = nameof(DistributedCacheService);

      Installers.Add(processInstaller);
      Installers.Add(installer);
    }

    public override void Install(IDictionary stateSaver)
    {
      // Выдать права на запись логов.
      var currentDirectory = Path.GetPathRoot(typeof(ServiceInstaller).Assembly.Location);
      var logs = Path.Combine(currentDirectory, "logs");
      bool exists = System.IO.Directory.Exists(logs);
      if (!exists)
        System.IO.Directory.CreateDirectory(logs);
      var dInfo = new DirectoryInfo(logs);
      var dSecurity = dInfo.GetAccessControl();
      dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
      dInfo.SetAccessControl(dSecurity);

      base.Install(stateSaver);
    }
  }
}
