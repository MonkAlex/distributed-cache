﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;

namespace DistributedCache.Models
{
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single, IncludeExceptionDetailInFaults = true)]
  [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
  public class MemoryDistributedCache : ICache
  {
    protected Dictionary<string, byte[]> Storage = new Dictionary<string, byte[]>();
    
    public Stream Get(string key)
    {
      if (key == null)
      {
        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
        return null;
      }

      byte[] result;
      if (Storage.TryGetValue(key, out result))
        return new MemoryStream(result);

      var peers = Peer.Peers.ToList();
      var peerWithValue = peers.FirstOrDefault(p => p.Cache.Contains(key));
      if (peerWithValue != null)
        return peerWithValue.Cache.Get(key);

      WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
      return null;
    }

    public void Set(string key, Stream value)
    {
      if (key == null)
      {
        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
        return;
      }

      using (var memory = new MemoryStream())
      {
        value.CopyTo(memory);
        Storage[key] = memory.ToArray();
      }
    }

    public void Remove(string key)
    {
      if (key == null)
      {
        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
        return;
      }

      Storage.Remove(key);
    }

    public bool Contains(string key)
    {
      if (key == null)
      {
        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
        return false;
      }

      return Storage.ContainsKey(key);
    }
  }
}