﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace DistributedCache.Models
{
  [ServiceContract(Name = "Cache")]
  public interface ICache
  {
    [OperationContract]
    [WebGet(UriTemplate = "/data/{key}",
      BodyStyle = WebMessageBodyStyle.Bare)]
    Stream Get(string key);

    [OperationContract]
    [WebInvoke(UriTemplate = "/data/{key}",
      BodyStyle = WebMessageBodyStyle.Bare,
      Method = "POST")]
    void Set(string key, Stream value);

    [OperationContract]
    [WebInvoke(UriTemplate = "/data/{key}",
      BodyStyle = WebMessageBodyStyle.Bare,
      Method = "DELETE")]
    void Remove(string key);

    [OperationContract]
    [WebGet(UriTemplate = "/data/contains={key}",
      BodyStyle = WebMessageBodyStyle.Bare)]
    bool Contains(string key);
  }
}