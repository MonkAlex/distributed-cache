using System;
using System.ServiceProcess;

namespace DistributedCache
{
  class Program
  {
    static void Main(string[] args)
    {
      if (Environment.UserInteractive)
      {
        var client = new Client();
        try
        {
          client.Start();
          Console.ReadLine();
        }
        finally
        {
          client.Stop();
        }
      }
      else
      {
        ServiceBase[] ServicesToRun;
        ServicesToRun = new ServiceBase[]
        {
          new DistributedCacheService(),
        };
        ServiceBase.Run(ServicesToRun);
      }
    }

  }
}
