﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using DistributedCache;
using DistributedCache.Models;
using NUnit.Framework;
using MemoryStream = System.IO.MemoryStream;

namespace distributed_cache.tests
{
  [TestFixture]
  public class SetGetRemoveFromController
  {
    [Test]
    public async Task SetGetRemove()
    {
      MemoryDistributedCache DemoServices = new MemoryDistributedCache();
      WebServiceHost _serviceHost = new WebServiceHost(DemoServices, new Uri("http://localhost:8000"));
      _serviceHost.Open();
      
      var remoteCache = new RemoteCache("http://localhost:8000/data");
      var client = new HttpClient();
      string url = "http://localhost:8000/data/2";

      using (var response = await client.GetAsync(url))
      {
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
      }

      var test = "test";
      var content = new StreamContent(new MemoryStream(Encoding.Unicode.GetBytes(test)));
      using (var response = await client.PostAsync(url, content))
      {
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
      }

      using (var response = await client.GetAsync(url))
      {
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        Assert.AreEqual(Encoding.Unicode.GetString(await response.Content.ReadAsByteArrayAsync()), test);
      }

      Assert.IsTrue(remoteCache.Contains("2"));
      Assert.IsFalse(remoteCache.Contains("3"));
      Assert.AreEqual(Encoding.Unicode.GetString((remoteCache.Get("2") as MemoryStream).ToArray()), test);

      using (var response = await client.DeleteAsync(url))
      {
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
      }

      using (var response = await client.GetAsync(url))
      {
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
      }
    }
  }
}