﻿using System.ServiceProcess;

namespace DistributedCache
{
  partial class DistributedCacheService : ServiceBase
  {
    private Client client;

    public DistributedCacheService()
    {
      InitializeComponent();
      // Пауза и возобновление - не реализованы.
      this.CanPauseAndContinue = false;
      this.CanStop = true;
      this.AutoLog = true;
    }

    protected override void OnStart(string[] args)
    {
      client = new Client();
      client.Start();
    }

    protected override void OnStop()
    {
      client?.Stop();
    }
  }
}
