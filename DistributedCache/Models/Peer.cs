﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.PeerToPeer;
using System.Text;

namespace DistributedCache.Models
{
  public class Peer
  {
    public static List<Peer> Peers = new List<Peer>();

    public PeerNameRecord Record { get; }

    public ICache Cache { get; }

    public IPEndPoint Address { get; }

    public Peer(PeerNameRecord record, ICache cache, IPEndPoint address)
    {
      this.Record = record;
      this.Cache = cache;
      this.Address = address;
    }
  }
}
