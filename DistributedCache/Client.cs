﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.PeerToPeer;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using DistributedCache.Models;

namespace DistributedCache
{
  public class Client
  {
    private static int HostPort = 8000;
    private static string url = $"http://0.0.0.0:{HostPort}";

    private WebServiceHost serviceHost;
    private PeerNameRegistration nameRegistration;
    private Timer timer;

    public void Start()
    {
      var distributedCache = new MemoryDistributedCache();

      try
      {
        serviceHost = new WebServiceHost(distributedCache, new Uri(url));
        serviceHost.Open();
        var peerName = new PeerName("MyName", PeerNameType.Unsecured);
        nameRegistration = new PeerNameRegistration(peerName, HostPort) { Cloud = Cloud.AllLinkLocal };
        nameRegistration.Start();

        timer = new Timer(Timer, null, 0, 10000);
      }
      catch (AddressAlreadyInUseException e)
      {
        this.Stop();
        Logger.Error(e);
      }
    }

    public void Stop()
    {
      Logger.Debug("===== END =====");
      nameRegistration?.Stop();
      serviceHost?.Close();
      timer?.Dispose();
    }

    private static void Timer(object state)
    {
      Peer.Peers.Clear();
      Logger.Debug("Research peers...");
      var nameResolver = new PeerNameResolver();
      nameResolver.ResolveProgressChanged += NameResolverOnResolveProgressChanged;
      nameResolver.ResolveCompleted += NameResolverOnResolveCompleted;
      nameResolver.ResolveAsync(new PeerName("0.MyName"), 1);
    }

    private static void NameResolverOnResolveCompleted(object sender, ResolveCompletedEventArgs e)
    {
      foreach (var peerNameRecord in e.PeerNameRecordCollection)
      {
        foreach (var point in peerNameRecord.EndPointCollection.Where(ep => ep.AddressFamily == AddressFamily.InterNetwork))
        {
          try
          {
            var remoteCache = new RemoteCache($"http://{point.Address}:{point.Port}/data");
            Peer.Peers.Add(new Peer(peerNameRecord, remoteCache, point));
            Logger.Debug($"Add client from {point}");
          }
          catch (EndpointNotFoundException)
          {
            Logger.Error("Found peer without endpoint : " + peerNameRecord.PeerName);
          }
        }
      }
    }

    private static void NameResolverOnResolveProgressChanged(object sender, ResolveProgressChangedEventArgs e)
    {
    }
  }
}
